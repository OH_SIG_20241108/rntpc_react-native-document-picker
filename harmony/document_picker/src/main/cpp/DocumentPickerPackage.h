/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the MIT License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://gitee.com/openharmony-sig/rntpc_react-native-document-picker
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include "generated/RNOH/generated/BaseReactNativeDocumentPickerPackage.h"

namespace rnoh {
class DocumentPickerPackage : public BaseReactNativeDocumentPickerPackage {
    using Super = BaseReactNativeDocumentPickerPackage;
    using Super::Super;
};
} // namespace rnoh